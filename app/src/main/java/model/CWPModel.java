package model;

import android.util.Log;

import java.io.IOException;
import java.util.Observable;

import cwprotocol.CWPControl;
import cwprotocol.CWPMessaging;
import cwprotocol.CWProtocolImplementation;
import cwprotocol.CWProtocolListener;

public class CWPModel extends Observable implements CWPMessaging, CWPControl, CWProtocolListener {

    CWProtocolImplementation cwProtocolImplementation = new CWProtocolImplementation(this);


    @Override
    public void lineUp() throws IOException {
        cwProtocolImplementation.lineUp();
        setChanged();
        notifyObservers(cwProtocolImplementation.getCurrentState());
    }

    @Override
    public void lineDown() throws IOException {
       cwProtocolImplementation.lineDown();
        setChanged();
        notifyObservers(cwProtocolImplementation.getCurrentState());


    }

    @Override
    public void connect(String serverAddr, int serverPort, int frequency) throws IOException {
       cwProtocolImplementation.connect(serverAddr, serverPort, frequency);
       setChanged();
        notifyObservers(cwProtocolImplementation.getCurrentState());
    }

    @Override
    public void disconnect() throws IOException {
        cwProtocolImplementation.disconnect();
        setChanged();
        notifyObservers(cwProtocolImplementation.getCurrentState());
    }

    @Override
    public boolean isConnected() {
        return cwProtocolImplementation.isConnected();
    }

    @Override
    public void setFrequency(int frequency) throws IOException {
        cwProtocolImplementation.setFrequency(frequency);
    }

    @Override
    public int frequency() {
        return cwProtocolImplementation.frequency();
    }

    @Override
    public boolean lineIsUp() {
        return cwProtocolImplementation.lineIsUp();
    }

    @Override
    public boolean serverSetLineUp() {
        return false;
    }

    @Override
    public void onEvent(CWProtocolListener.CWPEvent event, int param) {
        setChanged();
        notifyObservers(event);

    }
}
