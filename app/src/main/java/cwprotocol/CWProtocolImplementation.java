package cwprotocol;

import android.os.ConditionVariable;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Observer;
import java.util.concurrent.Semaphore;


public class CWProtocolImplementation implements CWPControl, CWPMessaging, Runnable {
    private static final String TAG = "fi_oulu_tol_CWPImpltion";
    private static final int BUFFER_LENGTH = 64;
    private CWPState currentState = CWPState.Disconnected;
    private CWPState nextState = currentState;
    private int ServerFrequency = DEFAULT_FREQUENCY;
    private Handler receiveHandler = new Handler();
    private CWProtocolListener cwProtocolListener;
    private OutputStream nos = null; //Network Output Stream
    private ByteBuffer outBuffer = null;
    private String serverAddr = null;
    private int serverPort = -1;
    private CWPConnectionWriter writer = null;
    private android.os.ConditionVariable condVar;
    private int messageInt = 0;
    private short messageShort = 0;
    private long lineUpStart = 0;
    private long connectionTime = 0;
    private short lineStatus = 0;
    private int frequency;
    private CWPConnectionReader reader = null;
    private Thread thread;
    private Semaphore lock = new Semaphore(1);

    public CWProtocolImplementation(CWProtocolListener listener_p) {
        this.cwProtocolListener = listener_p;
        condVar = new ConditionVariable();
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {

        switch (nextState) {

            case Connected: {
                Log.d(TAG, "State change to connected happening...");
                currentState = nextState;
                cwProtocolListener.onEvent(CWProtocolListener.CWPEvent.EConnected, 0);

                break;
            }
            case Disconnected: {
                Log.d(TAG, "State change to Disconnected happening...");
                currentState = nextState;
                cwProtocolListener.onEvent(CWProtocolListener.CWPEvent.EDisconnected, 0);
                break;
            }
            case LineUp: {
                Log.d(TAG, "State change to lineup happening...");
                currentState = nextState;
                cwProtocolListener.onEvent(CWProtocolListener.CWPEvent.ELineUp, 0);
                break;
            }
            case LineDown: {
                Log.d(TAG, "State change to lineDown happening...");
                currentState = nextState;
                cwProtocolListener.onEvent(CWProtocolListener.CWPEvent.ELineDown, 0);
                break;
            }
            default: {
                break;
            }
        }
        //lock.release();

    }

    @Override
    public void addObserver(Observer observer) {

    }

    @Override
    public void deleteObserver(Observer observer) {

    }

    @Override
    public void lineUp() {
        lineUpStart = System.currentTimeMillis();
        messageInt = (int) (lineUpStart - connectionTime);
        Log.d(TAG, "Connection time: " + connectionTime);
        Log.d(TAG, "Line up called. messageInt=" + messageInt);
        condVar.open();
        if (currentState == CWPState.LineDown) {
            reader.changeProtocolState(CWPState.LineUp);
        }

    }

    @Override
    public void lineDown() {

        messageShort = (short) (System.currentTimeMillis() - lineUpStart);
        Log.d(TAG, "Line down called. messageShort=" + messageShort);
        condVar.open();
        if (currentState == CWPState.LineUp) {
            reader.changeProtocolState(CWPState.LineDown);
        }
    }

    @Override
    public void connect(String serverAddr, int serverPort, int frequency) {
        Log.e(TAG, "Connection function. frequency=" + frequency);
        this.serverAddr = serverAddr;
        this.serverPort = serverPort;
        this.frequency = frequency;

        reader = new CWPConnectionReader(this);
        reader.startReading();
        writer = new CWPConnectionWriter();
        writer.startSending();
        messageInt = frequency;
        reader.changeProtocolState(CWPState.Connected);
        connectionTime = System.currentTimeMillis();
    }

    @Override
    public void disconnect() {

        writer.stopSending();
        Log.d(TAG, "Writer stopped sending");
        writer = null;
        reader.changeProtocolState(CWPState.Disconnected);
        if (reader != null) {
                reader.stopReading();
            try {
                reader.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            reader = null;

        }
        this.serverAddr = null;
        this.serverPort = -1;
        this.frequency = DEFAULT_FREQUENCY;


    }

    public void sendFrequency(int freq) {
        messageInt = freq;
        condVar.open();
        if (currentState == CWPState.Connected) {
            reader.changeProtocolState(CWPState.Connected);

        }
    }

    @Override
    public boolean isConnected() {
        return currentState == CWPState.Connected;
    }

    @Override
    public boolean lineIsUp() {

        return currentState == CWPState.LineUp;


    }

    @Override
    public boolean serverSetLineUp() {
        return false;
    }

    @Override
    public void setFrequency(int frequency) {
        //if positive change to negative
        if (frequency > 0) {
            frequency = frequency * -1;
        }
        if (frequency != ServerFrequency) {
            Log.d(TAG, "freq_debug_current: " + ServerFrequency);
            Log.d(TAG, "freq_debug_next: " + frequency);
            sendFrequency(frequency);

        }
    }

    @Override
    public int frequency() {
        return ServerFrequency;
    }

    public CWProtocolListener.CWPEvent getCurrentState() {
        switch (currentState) {
            case Connected: {
                return CWProtocolListener.CWPEvent.EConnected;
            }
            case Disconnected: {
                return CWProtocolListener.CWPEvent.EDisconnected;

            }
            case LineUp: {
                return CWProtocolListener.CWPEvent.ELineUp;
            }
            case LineDown: {
                return CWProtocolListener.CWPEvent.ELineDown;
            }
            default: {
                return null;
            }

        }
    }

    public void setCurrentState(CWPState currentState) {
        this.currentState = currentState;
    }

    public enum CWPState {Disconnected, Connected, LineDown, LineUp}

    private class CWPConnectionReader extends Thread {
        private static final String TAG = "fi_oulu_tol_CWPReader";
        private volatile boolean running = false;
        private Runnable myProcessor = null;
        private Socket cwpSocket = null;
        private InputStream nis = null; //Network Input Stream


        private CWPConnectionReader(Runnable processor) {
            this.myProcessor = processor;


        }

        void startReading() {
            Log.d(TAG, this.getName());
            this.running = true;
            start();

        }

        void stopReading() {
            running = false;
            nis = null;
            nos = null;
            cwpSocket = null;
            nextState = CWPState.Disconnected;


        }

        private void doInitialize() throws IOException {


            SocketAddress sockAddr = new InetSocketAddress(serverAddr, serverPort);
            cwpSocket = new Socket();
            cwpSocket.connect(sockAddr, 5000);
            if (cwpSocket.isConnected()) {
                Log.d(TAG, "socket connected");
            }
            nis = cwpSocket.getInputStream();
            nos = cwpSocket.getOutputStream();


        }

        @Override
        public void run() {
            super.run();
            try {
                doInitialize();
                int bytesToRead = 4;
                int bytesRead = 0;
                outBuffer = ByteBuffer.allocate(BUFFER_LENGTH);
                outBuffer.order(ByteOrder.BIG_ENDIAN);
                byte[] byteArray = new byte[4];
                while (running) {
                    Log.d(TAG, "Running!");
                    bytesRead = readLoop(byteArray, bytesToRead);
                    Log.d(TAG, "Read loop!");
                    outBuffer.clear();
                    outBuffer.put(byteArray);
                    outBuffer.position(0);
                    int value = outBuffer.getInt();
                    Log.d(TAG, "Read int value: " + value);
                    if (value < 0) {
                        ServerFrequency = value;
                        if (ServerFrequency != frequency) {
                            sendFrequency(frequency);
                        }
                        Log.d(TAG, "Frequency: " + value);
                        nextState = CWPState.LineDown;
                        myProcessor.run();
                    } else {
                        Log.d(TAG, "bytes read: " + bytesRead);
                        if (bytesRead == 2) {
                            nextState = CWPState.LineDown;
                            myProcessor.run();
                        }
                        if (bytesRead == 4) {
                            nextState = CWPState.LineUp;
                            myProcessor.run();

                        }
                    }
                }
                Log.d(TAG, "Reader thread loop closed");
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        private void changeProtocolState(CWPState state) {
            Log.e(TAG, "Change protocol state to " + state);
            //lock.acquire();
            nextState = state;
            //messageValue = param;
            if (state == CWPState.LineUp || state == CWPState.LineDown) {
                condVar.open();
            }
            receiveHandler.post(myProcessor);
        }

        private int readLoop(byte[] bytes, int bytesToRead) throws IOException {
            int readNow = 0;

            do {
                readNow = nis.read(bytes, 0, bytesToRead);
                Log.d(TAG, "Read a byte");
                if (readNow == -1) { // end of stream, server closed the connection.
                    throw new IOException("Read -1 from stream"); // You should implement this too!

                }

            }
            while (bytes.length < bytesToRead);


            return readNow;
        }
    }

    private class CWPConnectionWriter extends Thread {
        final String TAG = "fi_oulu_tol_CWP_Writer";
        private boolean running = false;

        void startSending() {
            this.running = true;
            start();
            Log.d(TAG, this.getName());
        }

        void stopSending() {
            running = false;
            condVar.open();
        }


        @Override
        public void run() {
            super.run();
            while (running) {
                try {
                    condVar.block();
                    if (messageInt != 0) {
                        Log.d("fi_oulu_sending", "messInt: " + messageInt);
                        sendMessage(messageInt);
                    } else if (messageShort > 0) {
                        Log.d("fi_oulu_sending", "messShort: " + messageShort);
                        sendMessage(messageShort);
                    }

                    messageInt = 0;
                    messageShort = 0;
                    condVar.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            Log.d(TAG, "Writer thread loop closed");
        }

        private void sendMessage(int msg) throws IOException {
            Log.d(TAG, "sending int message:" + msg);
            //Log.d(TAG, "Message binary string: "+Integer.toBinaryString(msg));
            ByteBuffer byteBuffer = ByteBuffer.allocate(4);
            byteBuffer.order(ByteOrder.BIG_ENDIAN);
            byteBuffer.putInt(msg);
            byteBuffer.position(0);
            byte[] bytesArray = byteBuffer.array();
            nos.write(bytesArray);
            nos.flush();
            byteBuffer = null;
            bytesArray = null;

        }

        private void sendMessage(short msg) throws IOException {
            Log.d(TAG, "sending short message:" + msg);
            ByteBuffer byteBuffer = ByteBuffer.allocate(2);
            byteBuffer.order(ByteOrder.BIG_ENDIAN);
            byteBuffer.putShort(msg);
            byteBuffer.position(0);
            byte[] bytesArray = byteBuffer.array();
            nos.write(bytesArray);
            nos.flush();
            byteBuffer = null;
            bytesArray = null;
        }
    }

}
