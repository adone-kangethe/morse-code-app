package cwprotocol;

import java.io.IOException;
import java.util.Observer;

public interface CWPControl {
    int DEFAULT_FREQUENCY = -1;

    void addObserver(Observer observer);
    void deleteObserver(Observer observer);
    // Connection management
    void connect(String serverAddr, int serverPort, int frequency) throws IOException;
    void disconnect()  throws IOException;
    boolean isConnected();
    // Frequency management
    void setFrequency(int frequency) throws IOException;
    int frequency();
}
