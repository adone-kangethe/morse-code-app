package fi.oulu.tol.esde27.cwpclient27;

import cwprotocol.CWPControl;
import cwprotocol.CWPMessaging;

public interface CWPProvider {
    CWPMessaging getMessaging();
    CWPControl getControl();
}
