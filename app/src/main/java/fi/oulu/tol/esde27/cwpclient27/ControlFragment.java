package fi.oulu.tol.esde27.cwpclient27;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import cwprotocol.CWPControl;


public class ControlFragment extends Fragment implements Observer {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    // TODO: Rename and change types of parameters

    ToggleButton control_button;
    CWPControl control;
    EditText frequency_edit;
    Button changeFrequencyButton;
    private final String TAG = "fi_oulu_tol_CtrllFrgmnt";
    SharedPreferences sharedPref;

    public ControlFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=  inflater.inflate(R.layout.fragment_control, container, false);
        frequency_edit = v.findViewById(R.id.freqEdit);
        final int freq= Integer.parseInt(sharedPref.getString("frequency", "-1"));
        frequency_edit.setText(freq+"");
        control_button = v.findViewById(R.id.controlToggleBttn);
        control_button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Log.d(TAG, control_button.isChecked()+"");
                if (control_button.isChecked()){
                    try {
                        Log.e(TAG, "button clicked. frequency="+freq);
                        control.connect("10.0.0.149", 20000, freq);
                        Log.d(TAG, "Connection initialized");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }
                else {
                    try {
                        control.disconnect();
                        Log.d(TAG, "Connection deleted");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }


        });
        changeFrequencyButton = v.findViewById(R.id.chngeFreqBttn);
        changeFrequencyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFrequency();
            }
        });
        return v;

    }
    private void changeFrequency(){
        int freq = Integer.parseInt(frequency_edit.getText().toString());
        try {
            control.setFrequency(freq);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Get the new freq from the ControlFragment widget when button pressed to newFreq variable

        SharedPreferences.Editor edit = sharedPref.edit();
        edit.putString("frequency", Integer.toString(freq));
        edit.commit();

    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        CWPProvider p = (CWPProvider) getActivity();
        control = p.getControl();
        control.addObserver(this);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(
                getActivity().getApplicationContext());

    }

    @Override
    public void onDetach() {
        super.onDetach();
        control = null;
        control.deleteObserver(this);

    }


    @Override
    public void update(Observable o, Object arg) {

        /*Toast.makeText(getActivity().getApplicationContext(),
                getString(R.string.connecting_cwp),
                Toast.LENGTH_SHORT).show();*/
        Log.d(TAG, arg.toString());

    }
}
