package fi.oulu.tol.esde27.cwpclient27;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import cwprotocol.CWPMessaging;
import cwprotocol.CWProtocolImplementation;
import cwprotocol.CWProtocolListener;
import model.CWPModel;



public class TappingFragment extends Fragment implements Observer{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private ImageView statusImage;
    private TextView userStateText;
    private TextView serverStatetext;
    private CWPMessaging cwpMessenger;
    private final String TAG = "fi_oulu_tol_TAP_FRGNT";
    private Runnable updateUi;
    CWProtocolListener.CWPEvent state;

    // TODO: Rename and change types of parameters




    public TappingFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tapping, container, false);
        userStateText = v.findViewById(R.id.userLineState);
        statusImage = v.findViewById(R.id.statusImageView);
        /*statusImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    cwpMessenger.lineUp();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                switch (state){
                    case ELineUp: {
                        try {
                            cwpMessenger.lineDown();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                    case ELineDown:{
                        try {
                            cwpMessenger.lineUp();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    }

                }
                }

        });*/
        statusImage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //Log.d(TAG, event.getAction()+"");
                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:{
                        try {
                            Log.d(TAG, "Down");
                            cwpMessenger.lineUp();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                    case MotionEvent.ACTION_UP:{
                        try {
                            Log.d(TAG, "up");
                            cwpMessenger.lineDown();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                }


                return true;
            }
        });


        return v;

    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        CWPProvider provider = (CWPProvider) getActivity();
        cwpMessenger = provider.getMessaging();
        cwpMessenger.addObserver(this);
        updateUi = new Runnable() {
            @Override
            public void run() {
                switch (state) {
                    case ELineUp: {
                        statusImage.setImageResource(R.mipmap.hal9000_up);
                        userStateText.setText("O");
                        break;
                    }
                    case ELineDown: {
                        statusImage.setImageResource(R.mipmap.hal9000_down);
                        userStateText.setText("●");
                        break;
                    }
                    case EDisconnected: {
                        statusImage.setImageResource(R.mipmap.hal9000_offline);
                        userStateText.setText("O");
                        break;
                    }

                }
            }
        };

    }

    @Override
    public void onDetach() {
        super.onDetach();
        cwpMessenger.deleteObserver(this);
        cwpMessenger = null;

    }


     @Override
    public void update(Observable o, Object arg) {
        Log.d(TAG, "update available");

        Log.d(TAG, arg.getClass().getName());
        state = (CWProtocolListener.CWPEvent) arg;
        Log.d(TAG, state.name());
        getActivity().runOnUiThread(updateUi);

    }





}
